// REW Auto EQ to GEQ

function writeTextFile(afilename, output) {
  var txtFile = new File(afilename);
  txtFile.writeln(output);
  txtFile.close();
}

function makeArr(startValue, stopValue, cardinality) {
  var arr = [];
  var step = (stopValue - startValue) / (cardinality - 1);
  for (var i = 0; i < cardinality; i++) {
    arr.push(startValue + (step * i));
  }
  return arr;
}


function peakfilt(x, fc, fb, G, fs) {
  let H0 = 10 ** (G / 20) - 1;
  let b = (Math.tan(Math.PI * fb / fs) - 1) / (Math.tan(Math.PI * fb / fs) + 1);
  let c = -Math.cos(2 * Math.PI * fc / fs);
  let len = x.length + 2;
  let y1 = new Array(len).fill(0);
  let y = new Array(len).fill(0);
  let a = [0, 0];
  let in2 = a.concat(x);


  for (let ii = 2; ii < len; ii++) {
    y1[ii] = -b * in2[ii] + c * (1 - b) * in2[ii - 1] + in2[ii - 2] - c * (1 - b) * y1[ii - 1] + b * y1[ii - 2];
    y[ii] = H0 / 2 * (in2[ii] - y1[ii]) + in2[ii];
  }
  y.splice(0, 1);
  y.splice(0, 1);
  return y
}


var xyValues = [{ x: 50, y: 7 }];
var fGEQ = [20, 25, 31, 40, 50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000, 1250, 1600, 2000, 2500, 3150, 4000, 5000, 6300, 8000, 10000, 12500, 16000, 20000];

var text = [];
var result = [];
var position = [];
var f = [];
var gain = [];
var q = [];
var h = new Array(2048).fill(0);
h[0] = 1;
var HRe = [];
var HIm = [];
var HAbs = [];
var HGEQ = [];

document.getElementById('inputfile')
  .addEventListener('change', function () {

    var fr = new FileReader();
    fr.onload = function () {
      document.getElementById('output').textContent = fr.result;
      text = fr.result;

      for (let ii = 1; ii <= 21; ii++) {
        if (ii < 10) {
          let filter = "Filter  ";
          position[ii - 1] = text.indexOf(filter.concat(ii.toString()));
        } else {
          let filter = "Filter ";
          position[ii - 1] = text.indexOf(filter.concat(ii.toString()));
        }

      }

      for (let ii = 1; ii <= 20; ii++) {
        result[ii - 1] = text.substring(position[ii - 1], position[ii] - 2);
      }

      var positionFc = result[0].indexOf('Fc');
      var positionHz = result[0].indexOf('Hz');
      var positionGain = result[0].indexOf('Gain');
      var positiondB = result[0].indexOf('dB');
      var positionQ = result[0].indexOf('Q');

      for (let ii = 1; ii <= 20; ii++) {
        if (result[ii - 1].indexOf("None") != -1) {
          break;
        }
        f[ii - 1] = parseFloat(result[ii - 1].substring(positionFc + 2, positionHz));
        gain[ii - 1] = parseFloat(result[ii - 1].substring(positionGain + 4, positiondB));
        q[ii - 1] = parseFloat(result[ii - 1].substring(positionQ + 1, result[ii - 1].length));
      }

      for (let ii = 0; ii < f.length; ii++) {
        h = peakfilt(h, f[ii], f[ii] / q[ii], gain[ii], 48000);
      }

      var a = document.body.appendChild(
        document.createElement("a")
      );
      a.download = "export.json";
      a.href = "data:text/plain;base64," + btoa(JSON.stringify(h));
      a.innerHTML = "download example text";

      // DFT
      var N = h.length;
      var dataArray = [];
      var fq = makeArr(0, 48000 - 48000 / N, N);
      HRe = new Array(N).fill(0);
      HIm = new Array(N).fill(0);
      for (let ii = 0; ii < N; ii++) {
        for (let jj = 0; jj < N; jj++) {
          HRe[ii] = HRe[ii] + h[jj] * Math.cos(Math.PI * 2 * jj * ii / N);
          HIm[ii] = HIm[ii] - h[jj] * Math.sin(Math.PI * 2 * jj * ii / N);
        }
        HAbs[ii] = 20 * Math.log10(Math.sqrt(HRe[ii] * HRe[ii] + HIm[ii] * HIm[ii]));
        xyValues[ii] = new Object();
        xyValues[ii].x = fq[ii];
        xyValues[ii].y = HAbs[ii];
      }



      var data = {
        labels: fq,
        datasets: [{
          label: 'HAbs',
          data: xyValues,
          backgroundColor: "rgba(255,0,0,0.4)",
          borderColor: "rgba(255,0,0,1)",
          // this dataset is drawn below
          order: 1
        }]
      };

      var ctx = document.getElementById("myChart").getContext("2d");
      ctx.canvas.width = 500;
      ctx.canvas.height = 250;

      var myNewChart = new Chart(ctx, {
        type: 'line',
        heigth: 250,
        width: 500,
        data: data,
        options: {
          responsive: true,
          title: {
            display: true,
            text: 'Frequency Response of Filter'
          },
          scales: {
            yAxes: [{
              display: true,
            }],
            xAxes: [{
              ticks: {
                max: 24000
              },
              type: 'logarithmic',
              display: true
            }]
          }
        }
      }
      );

      for (let ii = 0; ii < fGEQ.length; ii++) {
        let fdiff = [];
        for (let jj = 0; jj < fq.length; jj++) {
          fdiff[jj] = Math.abs(fq[jj] - fGEQ[ii]);
        }
        let min = Math.min(...fdiff);
        let index = fdiff.indexOf(min);
        console.log(index);
        HGEQ[ii] = Math.round(HAbs[index] * 10) / 10;
      }


      var myTableDiv = document.getElementById("Table");

      var table = document.createElement('TABLE');
      table.border = '1';

      var tableBody = document.createElement('TBODY');
      table.appendChild(tableBody);

      var tr = document.createElement('TR');
      tableBody.appendChild(tr);
      var td = document.createElement('TD');
      td.width = '75';
      td.appendChild(document.createTextNode('f in Hz'));
      tr.appendChild(td);
      var td = document.createElement('TD');
      td.width = '75';
      td.appendChild(document.createTextNode('H in dB'));
      tr.appendChild(td);
      myTableDiv.appendChild(table);

      for (var ii = 0; ii < fGEQ.length; ii++) {
        var tr = document.createElement('TR');
        tableBody.appendChild(tr);

        for (var jj = 0; jj < 2; jj++) {
          var td = document.createElement('TD');
          td.width = '75';
          if (jj == 0) {
            td.appendChild(document.createTextNode(fGEQ[ii]));
          } else {
            td.appendChild(document.createTextNode(HGEQ[ii]));
          }

          tr.appendChild(td);
        }
      }
      myTableDiv.appendChild(table);
    }

    fr.readAsText(this.files[0]);
  })